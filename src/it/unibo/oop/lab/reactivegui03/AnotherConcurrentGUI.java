package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.Concurrent2GUI;

public class AnotherConcurrentGUI extends Concurrent2GUI{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public AnotherConcurrentGUI() {
        super();
        
        final Agent2 agent2 = new Agent2();
        new Thread(agent2).start();
       
    }
    
    private class Agent2 implements Runnable{
        @Override
        public void run() {
            try {
                 Thread.sleep(5000);
            }catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            
           
            
        }
        
    }
    

}

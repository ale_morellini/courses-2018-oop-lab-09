package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Concurrent2GUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    
    
    
    public Concurrent2GUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        
        final Agent agent = new Agent();
        new Thread(agent).start();
        /**
         * event handler associated to action event on button stop
         * @param e 
         *              the action event that will be handled by this listener
         */
        stop.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                
            }
        });
        
        /**
         * event handler associated to action event on button up
         * @param e 
         *              the action event that will be handled by this listener
         */
        up.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                agent.goingUp(); 
            }
        });
        
        /**
         * event handler associated on action event on button down
         * @param e 
         *              the action event that will be handled by this listener
         */
        down.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                agent.goingDown();
            }
        });
                
    }
    
//    public void stopCounting() {
//        
//        up.setEnabled(true);
//        down.setEnabled(true);
//    }

    
    private class Agent implements Runnable{

        private volatile boolean stop;
        /**
         * assume true for incrementing, false otherwise
         */
        private volatile boolean direction;
        private int counter;
        
        @Override
        public void run() {
            while(!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            Concurrent2GUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    incDec();
                    Thread.sleep(300);
                }catch (InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            
        }
        
        public void stopCounting() {
            this.stop = true;
        }
        
        public void goingUp() {
            this.direction = true;
        }
        
        public void goingDown() {
            this.direction = false;
        }
        
        private void incDec() {
            if(direction) {
                this.counter++;
            }else {
                this.counter--;
            }
        }
        
    }
}
